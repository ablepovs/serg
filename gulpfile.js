var gulp = require('gulp');
var less = require('gulp-less');
var watch = require('gulp-watch');
var prefix = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var browserSync = require("browser-sync").create();

gulp.task('serve', function() {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });
    gulp.watch('./*.less', ['less']);
    gulp.watch("public/*.html").on('change', browserSync.reload);
    gulp.watch("public/*.js").on('change', browserSync.reload);
});


gulp.task('less', function() {
    return gulp.src('style.less')
        .pipe(plumber())
        .pipe(less())
        .pipe(prefix('last 2 versions'))
        .pipe(gulp.dest('./public/css'))
        .pipe(browserSync.stream());
});


gulp.task('default', ['serve']);